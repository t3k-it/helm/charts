#!/bin/sh

ACTION=$1

[ -z $ACTION ] && printf "Please specify action [build, upload]\n" && exit 1

mkdir -p packages
	case $ACTION in
		"build")
			for chart in charts/*; do
				helm package $chart -d packages/
			done
			;;
		"upload")
			for chart in packages/*.tgz; do
				printf "Uploading %s to %s\n" "$chart" "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/helm/api/stable/charts"
				curl --verbose --request POST --user gitlab-ci-token:$CI_JOB_TOKEN --form "chart=@${chart}" "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/helm/api/stable/charts"
			done
			;;
		*)
			printf "Wrong action: ${ACTION}\nShould be one of [build, upload]\n" && exit 1
			;;
	esac
